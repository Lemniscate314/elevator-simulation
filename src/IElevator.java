public interface IElevator {
    public void goUP(int Dest);
    public void goDown(int Dest);
    public void selectFloor();
}
