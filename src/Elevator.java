import java.util.ArrayList;
import java.util.Scanner;

public class Elevator implements IElevator {
    //variables
    ArrayList<Integer> listOfFloors;
    boolean isDoorOpen = false;
    final int maxP = 20; /** max passenger*/
    final int maxF = 20; /** max floors*/
    final int minP = 1; /** min passengers*/
    final int minF = 1; /** min floors*/
    private int currentFloor = 1;
    private int destinationFloor= 0;
    Scanner scanner = new Scanner(System.in);
    int[] destination_lists = new int[maxF];
    int passFloor = 0; /** Passenger floor, we will use this variable for asking passenger's floor*/
     int numOfPass = 0;


    public Elevator()
    {
        currentFloor = 0;
    }
    void startSimulations()
    {
        display("Welcome to Elevator");
        display("Please enjoy");
        delay(5000);
    }

    @Override
    public void goUp() {
        display(currentFloor++ + "F | Going up ...");
    }

    @Override
    public void goDown() {
        display(currentFloor-- + "F | Going down ...");

    }
    @Override
    public int selectFloor(int id) {
        boolean isValidEntry = false;
        int floor = 0;
        while(!isValidEntry) {
            display("Passenger #"+(id+1) + " enter your floor: ");
            floor = scanner.nextInt();
            if(floor < minF || floor > maxF)
            {
                display("Error. You have entered out of range floor. Valid[1-20]");

            }
            else if(floor == currentFloor)
            {
                display("You are already in the "+ currentFloor + "F.");
            }
            else {
                destination_lists[floor-1]++;
                isValidEntry =true;
            }
        }


        return floor;
    }

    @Override
    public void askPassenger() {
        isDoorOpen = false;
        display("Elevator opening ...");
        delay(1300);
        isDoorOpen = true;
        display(currentFloor +"F | How many passengers:");
        numOfPass = scanner.nextInt();
        if(numOfPass < minP || numOfPass > maxP)
        {
            display("Error. valid numer of passengers [1-20]");
            askPassenger();
        }
        else
        {
            listOfFloors = new ArrayList<>();
            for(int a =0; a <numOfPass;a++)
            {
               int floor =  selectFloor(a);
               if(!listOfFloors.contains(floor)) listOfFloors.add(floor);
            }
        }
        /*for(int a:destination_lists) display("All destinations: "+a);
        for (int a = 0; a < listOfFloors.size(); a++)
        {
            display("Displaying unique destinations: " +listOfFloors.get(a));
        }*/
        display("Elevator closing ...");
        delay(1500);
        isDoorOpen = false;
        initialize_elevator();

    }
    void initialize_elevator()
    {
        for(int a = 0; a < listOfFloors.size(); a++)
        {
            int shortest = findShortest();
            display("Next destination: " + shortest+ "F Passenger amount ("+ destination_lists[shortest-1] + ")");
            delay(1300);
            while(currentFloor < shortest) goUp();
            while(currentFloor > shortest) goDown();
            while(destination_lists[shortest-1] > 0)
            {
                display("Unloading passenger ("+destination_lists[shortest-1]--+ ") at " + currentFloor + "F ");
                delay(1300);
            }

  
        }
        askPassenger();
    }

    /** this is the algorithm, we solve for the shortest distance of where the elevator is to all of selected floor, the shortest, the first elevator*/
    int findShortest()
    {
        int shortest = Math.abs(currentFloor -listOfFloors.get(0));
        int id = 0;
        for(int a = 1; a < listOfFloors.size();a++)
        {
            if(shortest > Math.abs(currentFloor - listOfFloors.get(a)))
            {
                shortest = Math.abs(currentFloor - listOfFloors.get(a));
                id = 0;

            }
        }
        shortest = listOfFloors.get(id);
        return shortest;
    }
    public void delay(int ms)
    {
        try
        {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    void display(Object o)
    {
        System.out.println(o);
    }

 public static void main(String[] args)
 {
     Elevator elevator = new Elevator();
     elevator.askPassenger();
 }
}
